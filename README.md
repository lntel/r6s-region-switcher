# R6S Region Switcher

Rainbow Six Siege (R6S) Region Switcher allows players to easily swap which region they are playing in without altering files and with ease.

<img src="/image-1.PNG">

## Regions
- Ping Based (Selects best region based on ping)
- Us East
- Us Central
- Us South Central
- Brazil South
- Europe North
- Europe West
- Asia East
- Asia South East
- Australia East
- Japan West

## Download

[v1.0.0](/uploads/a01962baef0c8c776855fde5a3c7976d/R6_Region_Switcher.exe)