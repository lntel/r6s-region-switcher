﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace R6_RS
{
    public partial class Form1 : Form
    {
        private string settings_path;
        private Dictionary<string, string> regions;
        private string[] dirs;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            regions = new Dictionary<string, string>();

            regions.Add("Ping Based", "default");
            regions.Add("Us East", "eus");
            regions.Add("Us Central", "cus");
            regions.Add("Us South Central", "scus");
            regions.Add("Brazil South", "sbr");
            regions.Add("Europe North", "neu");
            regions.Add("Europe West", "weu");
            regions.Add("Asia East", "eas");
            regions.Add("Asia South East", "seas");
            regions.Add("Australia East", "eau");
            regions.Add("Japan West", "wja");

            foreach(string title in regions.Keys)
            {
                bunifuDropdown1.AddItem(title);
            }

            settings_path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\My Games\Rainbow Six - Siege";

            dirs = Directory.GetDirectories(settings_path);

            int index = 0;
            string region = CurrentRegion(dirs);

            foreach(string value in regions.Values)
            {
                if (value == region) break;
                index++;
            }

            bunifuDropdown1.selectedIndex = index;

        }

        private string CurrentRegion(string[] profiles)
        {
            using (var fileStream = File.OpenRead(String.Format(@"{0}\GameSettings.ini", profiles[0])))
            {
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, 128))
                {
                    int index = 0;
                    String line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        if(index == 134)
                        {
                            return line.Split('=')[1];
                        }
                        index++;
                    }
                }
            }

            return null;
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            string key;
            string[] content;

            foreach(string dir in dirs)
            {
                try
                {
                    content = File.ReadAllLines(String.Format(@"{0}\GameSettings.ini", dir));

                    key = bunifuDropdown1.selectedValue;

                    for (int i = 0; i < content.Length; i++)
                    {
                        if (content[i].Contains("DataCenterHint="))
                        {
                            content[i] = string.Format("DataCenterHint={0}", regions[key]);
                            break;
                        }
                    }

                    File.WriteAllLines(String.Format(@"{0}\GameSettings.ini", dirs[0]), content);

                    content = null;
                }
                catch(Exception ex)
                {
                    //
                }
            }
        }
    }
}
